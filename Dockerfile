FROM node:lts-alpine
RUN mkdir /procsea-front
WORKDIR /procsea-front
ADD package.json /procsea-front
RUN npm install
ADD . /procsea-front/
RUN npm run build
