=======
procsea
=======

Procsea is an open-source voting and survey platform for cities or regions'
inhabitants. It empowers your will of democracy by giving them the power to
decide for the sake of their local government.

Built using Nuxt_

To know more about the project itself, please see PROJECT_ (WIP)

.. _Nuxt: https://nuxtjs.org/
.. _PROJECT: https://gitlab.com/le-cercle-briochin/procsea/blob/master/PROJECT.rst

Quickstart
----------

Please see the general repository of the project here_ for instructions on how to run its complete suite (ie: Backend stack + Frontend).

.. _here: https://gitlab.com/le-cercle-briochin/procsea/tree/master

License
-------

Licensed under the Affero General Public License, for more informations see LICENSE_

.. _LICENSE: LICENSE
