import axios from "axios";

// TODO: Config
export function Client() {
  return axios.create({
    baseURL: "/api",
    withCredentials: false,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
};
