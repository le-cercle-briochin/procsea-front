module.exports = {
    head: {
    title: "procsea",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Open-source Agora" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  loading: { color: "#3B8070" },

  build: {
    /* extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    } */
  },

  axios: {
    baseURL: ""
  },

  chimera: {
    prefetch: "get",
    ssrPrefetch: true,
    ssrPrefetchTimeout: 2000
  },

  /*
   ** Modules
   */
  modules: ["nuxt-buefy"],
  plugins: ["~plugins/laue"]
};
