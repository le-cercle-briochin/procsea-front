import { Client } from "~/core/Client";

export default {
  uri: "/proposals/",
  listContributions: function(filters = null) {
    return Client().get(this.uri, filters);
  },
  getContribution: function(id) {
    Client().get(this.uri + id);
  }
};
