import { Client } from "~/core/Client";

export default {
  uri: "/places/",
  listPlaces: function(filters = null) {
    return Client().get(this.uri, filters);
  },
  getPlace: function(id) {
    Client().get(this.uri + id);
  }
};
