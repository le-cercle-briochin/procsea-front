import { Client } from "~/core/Client";

export default {
  uri: "/sections/",
  listSections: function(filters = null) {
    return Client().get(this.uri, filters);
  },
  getSection: function(id) {
    Client().get(this.uri + id);
  }
};
