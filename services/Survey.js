import { Client } from "~/core/Client";

export default {
  uri: "/surveys/",
  listSurveys: function(filters = null) {
    return Client().get(this.uri, filters);
  },
  getSurvey: function(id) {
    Client().get(this.uri + id);
  }
};
