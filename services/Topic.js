import { Client } from "~/core/Client";

export default {
  uri: "/topics/",
  listTopics: function(filters = null) {
    return Client().get(this.uri, filters);
  },
  getTopic: function(id) {
    Client().get(this.uri + id);
  }
};
