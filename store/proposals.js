import ContributionService from "~/services/Contribution";

export const state = () => ({
  proposals: [],
  errors: []
});

export const mutations = {
  setProposals(state, data) {
    state.proposals = data;
  },
  setProposal(state, data) {
    state.proposals = [data];
  },
  setErrs(state, errors) {
    state.errors = errors;
  }
};

export const actions = {
  async getProposals({ commit }, filters = null) {
    ContributionService.listContributions(filters)
      .then(response => {
        commit("setProposals", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  },
  async getProposal({ commit }, id) {
    ContributionService.getContribution(id)
      .then(response => {
        commit("setProposal", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  }
};
