import SectionService from "~/services/Section";

export const state = () => ({
  sections: [],
  error: null
});

export const mutations = {
  setSections(state, data) {
    state.proposals = data;
  },
  setSection(state, data) {
    state.proposals = [data];
  },
  setErr(state, error) {
    state.error = error;
  }
};

export const actions = {
  async getSections({ commit }, filters = null) {
    SectionService.listSections(filters)
      .then(response => {
        commit("setSections", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErr", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErr", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErr", {
            type: "message",
            error: error.message
          });
        }
      });
  },
  async getSection({ commit }, slug) {
    SectionService.getSection(slug)
      .then(response => {
        commit("setSection", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErr", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErr", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErr", {
            type: "message",
            error: error.message
          });
        }
      });
  }
};
