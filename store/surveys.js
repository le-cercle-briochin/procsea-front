import SurveyService from "~/services/Survey";

export const state = () => ({
  surveys: []
});

export const mutations = {
  setSurveys(state, data) {
    state.surveys = data;
  },
  setSurvey(state, data) {
    state.surveys = [data];
  },
  setErrs(state, errors) {
    state.errors = errors;
  }
};

export const actions = {
  async getSurveys({ commit }, filters = null) {
    SurveyService.listSurveys(filters)
      .then(response => {
        commit("setSurveys", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  },
  async getSurvey({ commit }, id) {
    SurveyService.getSurvey(id)
      .then(response => {
        commit("setSurvey", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  }
};
