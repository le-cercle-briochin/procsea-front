import PlaceService from "~/services/Place";
import TopicService from "~/services/Topic";

export const state = () => ({
  topics: [],
  places: [],
  selected: [],
  errors: []
});

export const mutations = {
  add(state, tag) {
    state.selected.push(tag);
  },
  remove(state, tag) {
    state.selected.splice(state.selected.indexOf(tag), 1);
  },
  setAvailablePlaces(state, places) {
    state.places = places;
  },
  setAvailableTopics(state, topics) {
    state.topics = topics;
  },
  setErrs(state, errors) {
    state.errors = errors;
  }
};

export const actions = {
  async getPlaces({ commit }, filters = null) {
    PlaceService.listPlaces(filters)
      .then(response => {
        commit("setAvailablePlaces", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  },
  async getTopics({ commit }, filters = null) {
    TopicService.listTopics(filters)
      .then(response => {
        commit("setAvailableTopics", response);
      })
      .catch(error => {
        if (error.response) {
          commit("setErrs", {
            type: "response",
            error: error.response
          });
        } else if (error.request) {
          commit("setErrs", {
            type: "request",
            error: error.request
          });
        } else {
          commit("setErrs", {
            type: "message",
            error: error.message
          });
        }
      });
  }
};
